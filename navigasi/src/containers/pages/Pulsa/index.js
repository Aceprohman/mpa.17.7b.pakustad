import 'react-native-gesture-handler';
import React, {Component} from 'react' ;
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import {Platform, StyleSheet, Text, View, Image, ScrollView, TouchableOpacity} from 'react-native'

const Pulsa  = (onPress) => {
	return (
			<View style={{backgroundColor:'white',width:360}}>
					
				<Text style={{marginLeft:150,marginTop:20,fontSize:15, color:'black',fontWeight:'bold'}}>Pulsa & Data</Text>

					<View style={{marginTop:30,height:35, width:360, backgroundColor:'#8A2BE2',borderRadius:5}}>
		 		<Text style={{marginLeft:20, marginTop:5,fontSize:14, fontWeight:'bold', color:'white',fontWeight:'bold'}}>Telkomsel</Text>
		 	</View>

		 	<Text style={{marginLeft:20, marginTop:25,fontSize:14, fontWeight:'bold', color:'grey',fontWeight:'bold'}}>Nomor Ponsel</Text>
		 		<View style={{marginHorizontal:18,height:30,marginTop: 15,backgroundColor:'gainsboro',borderRadius:10,borderColor:'gainsboro',borderWidth:3}}> 
		 		</View>

					<View style={{borderColor:'grey',borderWidth:1 ,flexDirection:'row',height:50,marginTop: 25,width:360}}>
						<TouchableOpacity style={{flex:1, alignItems:'center', justifyContent:'center'}}>
							<Text style={{marginLeft:30,fontSize:15, color:'grey',fontWeight:'bold'}}>Isi Pulsa</Text>
						</TouchableOpacity>
						<TouchableOpacity style={{flex:1, alignItems:'center', justifyContent:'center'}}>
							<Text style={{marginLeft:30,fontSize:15, color:'grey',fontWeight:'bold'}}>Paket Data</Text>
						</TouchableOpacity>	
					</View>
				
		 	</View>


		)
}

export default Pulsa;