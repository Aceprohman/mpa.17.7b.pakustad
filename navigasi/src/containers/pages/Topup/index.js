import 'react-native-gesture-handler';
import React, {Component} from 'react' ;
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import {Platform, StyleSheet, Text, View, Image, ScrollView, TouchableOpacity} from 'react-native'

const Topup = () => {
	return (
		<View style={{flex:1}}>

				<ScrollView style={{flex:1, backgroundColor:'white'}}>

			<View style={{backgroundColor:'#8A2BE2',width:360,height:100}}>
					<Text style={{marginLeft:20,fontSize:15, color:'white',fontWeight:'bold'}}>Top Up</Text>

					<View style={{flexDirection:'row',marginHorizontal:18,height:50,marginTop: 25,marginLeft:-10}}>
						<TouchableOpacity style={{flex:1, alignItems:'center', justifyContent:'center'}}>
							<Text style={{marginLeft:30,fontSize:15, color:'white',fontWeight:'bold'}}>Instan Top Up</Text>
						</TouchableOpacity>
						<TouchableOpacity style={{flex:1, alignItems:'center', justifyContent:'center'}}>
							<Text style={{marginLeft:30,fontSize:15, color:'white',fontWeight:'bold'}}>Metode Lain</Text>
						</TouchableOpacity>	
					</View>
			<View style={{marginTop:1,height:3,backgroundColor:'#00FFFF',marginRight:180}}></View>	
		 	</View>

		 	<View>	
		 		<Text style={{marginTop:10,marginLeft:20,fontSize:15, color:'black',fontWeight:'bold'}}>Top Up ke</Text>
		 		<View style={{marginHorizontal:18,height:60,marginTop: 15,backgroundColor:'white',borderRadius:10,borderColor:'gainsboro',borderWidth:3}}>
		 		    <View style={{height:20, width:35, backgroundColor:'#8A2BE2',borderRadius:2, marginLeft:5, marginTop:13}}>
		 		    	<Text style={{color:'white', fontWeight:'bold',marginLeft:2}}>OVO</Text>
		 		    </View>
		 		    <Text style={{marginLeft:20,fontSize:15, color:'black',fontWeight:'bold',marginLeft:60,marginTop:-30}}>OVO Cash</Text>
		 		    <Text style={{marginLeft:20,fontSize:12, color:'black',marginLeft:60}}>Balance Rp.0</Text>
		 		</View>
		 	</View>

		 	<View style={{marginTop:20,height:5,backgroundColor:'gainsboro'}}></View>

		 	<View>

		 		<Text style={{marginTop:10,marginLeft:20,fontSize:15, color:'black',fontWeight:'bold'}}>Pilih Nominal Top Up</Text>
		 		<View style={{flexDirection:'row',height:30,marginTop: 25,marginLeft:20}}>

		 			<View  style={{width:100, alignItems:'center',backgroundColor:'white',borderRadius:12,borderColor:'gainsboro',borderWidth:2}}>
		 		    	<Text style={{marginLeft:10,marginTop:5,fontSize:12, color:'black',justifyContent:'center'}}>Rp.100.000</Text>
		 			</View>

		 			<View  style={{ marginLeft:6, marginRight:6, width:100, alignItems:'center',backgroundColor:'white',borderRadius:12,borderColor:'gainsboro',borderWidth:2}}>
		 		    	<Text style={{marginLeft:10,marginTop:5,fontSize:12, color:'black',justifyContent:'center'}}>Rp.100.000</Text>
		 			</View>

		 			<View  style={{width:100, alignItems:'center',backgroundColor:'white',borderRadius:12,borderColor:'gainsboro',borderWidth:2}}>
		 		    	<Text style={{marginLeft:10,marginTop:5,fontSize:12, color:'black',justifyContent:'center'}}>Rp.100.000</Text>
		 			</View>

		 		</View>

		 		<Text style={{marginTop:10, marginLeft:20,fontSize:12, color:'grey',}}>Atau masukkan nilai top up disini</Text>
		 		<View style={{marginLeft:20,height:40, width:320, backgroundColor:'gainsboro',borderRadius:2, marginTop:13}}></View>

		 	</View>


		 	<View style={{marginTop:20,height:5,backgroundColor:'gainsboro'}}></View>

		 	<View>
		 		<Text style={{marginTop:10,marginLeft:20,fontSize:15, color:'black',fontWeight:'bold'}}>Kartu Debit</Text>
		 		<Image style={{width:200, height:100, marginTop:5, marginLeft:20}}source={require('../../../icon/debit.png')}/>

		 	</View>

		 		</ScrollView>


		 	<View>

		 		<View style={{marginTop:20,height:5,backgroundColor:'gainsboro'}}></View>
		 		<View style={{marginLeft:20,height:40, width:320, backgroundColor:'gainsboro',borderRadius:20, marginTop:13}}>
		 			<Text style={{marginTop:10, marginLeft:100,fontSize:15, color:'white',fontWeight:'bold'}}>Top Up Sekarang</Text>
		 		</View>

		 	</View>

		 
		</View>


		)
}

export default Topup;