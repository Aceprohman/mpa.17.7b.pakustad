import 'react-native-gesture-handler';
import React, {Component} from 'react' ;
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import {Platform, StyleSheet, Text, View, Image, ScrollView, TouchableOpacity} from 'react-native'

const Transfer  = (onPress) => {
	return (
		<View style={{flex:1, backgroundColor:'white'}}>

			<View style={{backgroundColor:'#8A2BE2',width:360,height:75}}>

				<Text style={{marginLeft:150,marginTop:20,fontSize:15, color:'white',fontWeight:'bold'}}>Transfer</Text>

			</View>

			<View>

				<Image style={{width:200, height:200, marginLeft:100}}source={require('../../../icon/transfer1.png')}/>
				<Text style={{marginLeft:75,marginTop:5,fontSize:17, color:'#8A2BE2',fontWeight:'bold'}}>Kirim Uang Semakin Mudah</Text>
			
			</View>

			<View>

				<View style={{marginHorizontal:18,height:60,marginTop: 15,backgroundColor:'white'}}>
		 		    <Image style={{width:50, height:50, marginLeft:10}}source={require('../../../icon/transfer1.png')}/>
		 		    <Text style={{marginLeft:20,fontSize:12, color:'black',marginLeft:60,marginTop:-45}}>Transfer hanya dengan nomor ponsel teman</Text>
		 		    <Text style={{marginLeft:20,fontSize:12, color:'black',marginLeft:60}}>Anda yang sudah terdaftar di OVO</Text>
		 		</View>

		 		<View style={{marginHorizontal:18,height:60,marginTop: 15,backgroundColor:'white'}}>
		 		    <Image style={{width:50, height:50, marginLeft:10}}source={require('../../../icon/transfer2.png')}/>
		 		    <Text style={{marginLeft:20,fontSize:12, color:'black',marginLeft:60,marginTop:-45}}>Transfer secara 'Real Time'</Text>
		 		  
		 		</View>

		 		<View style={{marginHorizontal:18,height:60,marginTop: 15,backgroundColor:'white'}}>
		 		    <Image style={{width:50, height:50, marginLeft:10}}source={require('../../../icon/transfer3.png')}/>
		 		    <Text style={{marginLeft:20,fontSize:12, color:'black',marginLeft:60,marginTop:-45}}>Transfer ke rekening Bank dengan</Text>
		 		    <Text style={{marginLeft:20,fontSize:12, color:'black',marginLeft:60}}>Biaya terjangkau</Text>
		 		</View>

			</View>
			
			<View>

		 		
		 		<View style={{marginLeft:20,height:30, width:320, backgroundColor:'#8A2BE2',borderRadius:5, marginTop:2}}>
		 			<Text style={{marginTop:5, marginLeft:125,fontSize:18, color:'white',fontWeight:'bold'}}>MULAI</Text>
		 		</View>

		 	</View>
		</View>


		)
}

export default Transfer;