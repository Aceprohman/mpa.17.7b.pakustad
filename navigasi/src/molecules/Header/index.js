import React from 'react';
import { View, Text, Image } from 'react-native';     

const Header = () => {
	return (
			<View>
				<Image style={{width:360,height:170,borderBottomRightRadius:40,borderBottomLeftRadius:40}}source={require('../../icon/background.png')}/>
				<Image style={{marginTop:-160,marginLeft:310,height:20,width:20}}source={require('../../icon/lonceng.png')}/>
				<Text style={{marginTop:-32,marginLeft:15,fontSize:30, color:'white',fontWeight:'bold'}}>OVO</Text>
				<Text style={{marginTop:15,marginLeft:15,fontSize:10, color:'white',fontWeight:'bold'}}>OVO Cash</Text>
				<Text style={{marginTop:2,marginLeft:15,fontSize:15, color:'white',fontWeight:'bold'}}>Rp</Text>
				<Text style={{marginTop:-25,marginLeft:40,fontSize:30, color:'white',fontWeight:'bold'}}>0</Text>
				<Text style={{marginTop:4,marginLeft:15,fontSize:10, color:'white',fontWeight:'bold'}}>OVO Points</Text>
				<Text style={{marginTop:-13,marginLeft:75,fontSize:10, color:'gold',fontWeight:'bold'}}>0</Text>
		 	</View>
		)
}
     

export default Header;