import React from 'react';
import { View, Text, Image } from 'react-native';     

const Footer = () => {
	return (
			<View style={{height:54,flexDirection:'row', backgroundColor:'white',borderTopWidth:1,borderTopColor:'gainsboro'}}>
			<View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
				<Image style={{width:26, height:26, marginTop:5}}source={require('../../icon/home.png')}/>
				<Text style={{fontSize:15, color:'grey'}}>Home</Text>
			</View>
			<View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
				<Image style={{width:26, height:26, marginTop:5}}source={require('../../icon/deals.png')}/>
				<Text style={{fontSize:15, color:'grey',marginTop:4}}>Deals</Text>
			</View>
			<View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
				<Image style={{width:50, height:50, marginTop:-20}}source={require('../../icon/scan.png')}/>
				<Text style={{fontSize:15, color:'grey',marginTop:4}}>Scan</Text>
			</View>
			<View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
				<Image style={{width:26, height:26, marginTop:5}}source={require('../../icon/finance.png')}/>
				<Text style={{fontSize:15, color:'grey',marginTop:4}}>Finance</Text>
			</View>
			<View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
				<Image style={{width:26, height:26, marginTop:5}}source={require('../../icon/profile.png')}/>
				<Text style={{fontSize:15, color:'grey',marginTop:4}}>Profile</Text>
			</View>
		
		</View>
		
		)
}
     

export default Footer;